// примеры работ
const examplesWork = [];

for(let i=0; i<5; i++) {
  const item = {
    order: i+1,
    img: `${i+1}.jpg`,
  }
  examplesWork.push(item);
}
// конец примеры работ

// главная слайд
const mainSl = [
	{
		title: 	'Производство \n металлоконструкций',
		posttitle: 'в минимальные сроки',
		img: '1.jpg'
	},
	{
		title: 	'Монтаж металлоконструкций',
		posttitle: 'любой сложности',
		img: '2.jpg'
	},
	{
		title: 	'Собственны парк техники',
		posttitle: 'для монтажа \n металлоконструкций',
		img: '3.jpg'
	},
	// {
	// 	title: 	'',
	// 	img: '4.jpg'
	// }
];

// конец гл.слайд

// партнеры

const partners = [];
for(i=0;i<9;i++) {
	const item = {
		order: i+1,
		img: `${i+1}.png`,
	}
	partners.push(item);

}


new Vue({
	el: '#root',
	data: {
		examplesWork,
		mainSl,
		partners,
		isMenu: false,
		isExample: false,
		currentImg: null,

	},
	methods: {
		showMenu(){
			this.isMenu = !this.isMenu;
		},
		showExample(index){
			if (window.innerWidth > 768){
				this.isExample = !this.isExample;
				this.currentImg = ++index;
			}
		},
		closeExample(){
			this.isExample = !this.isExample;
		},
		nextExample(currentImg){
			if (this.currentImg < this.examplesWork.length){
				this.currentImg = ++currentImg;
			}
			else {
				this.currentImg = 1;
			}
		},
		prevExample(currentImg){
			if (this.currentImg > 1){
				this.currentImg = currentImg - 1;
			}
			else {
				this.currentImg = this.examplesWork.length;
			}
		}


	}
})