<!DOCTYPE html>
<html>
    <head>
        <meta content="text/html; charset=utf-8" http-equiv="Content-Type">
        <meta content="index, follow" name="robots">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>
            СибСтальКонструкция — 
            <?php echo $_SERVER['HTTP_HOST'] ?>
            </title>
            <link href="favicon.ico" rel="shortcut icon" type="image/x-icon">
            <link href="css/style.css?<?php echo rand() ?>
            " rel="stylesheet" type="text/css"> 
            <link href="css/jquery.fancybox.min.css" rel="stylesheet" type="text/css">
            <link href="css/swiper.min.css" rel="stylesheet" type="text/css">
            <script src="js/jquery.min.js" type="text/javascript" defer></script>
            <script src="js/jquery.fancybox.min.js" type="text/javascript" defer></script>
            <script src="js/swiper.min.js" type="text/javascript" defer></script>
            <script src="https://api-maps.yandex.ru/2.1/?lang=ru-RU" type="text/javascript" defer></script>
            <script src="js/script.js" type="text/javascript" defer></script>
            <script src="js/vue.js" type="text/javascript"></script>
        </head>
        <body>
            <div id="root">
                <header>
                    <div class="fond">
                        <div class="hood">
                            <div class="wrap">
                                <div class="on">
                                    <div class="box">
                                        <div class="lf sc-content sc-content-_cnt_360283061780723065">
                                            <a href="#">
                                                <img src="img/logo-xs.png">
                                                <div class="logo-text sc-content sc-content-_cnt_16412475891798198280">
                                                    <p>РСК СибСтальКонструкция</p>
                                                    <p>производство монтаж</p>
                                                </div>
                                            </a>
                                        </div>
                                        <div @click="showMenu" class="mob-burger">
                                            <img src="img/burger-red.png" alt="">
                                        </div>
                                        <div class="cn sc-content sc-content-_cnt_20221906981778079983">
                                            <ul>
                                                <li> <a href="#main" class="go">Главная</a> </li>
                                                <li> <a href="#advantages" class="go">Наши приемущества</a> </li>
                                                <li> <a href="#on" class="go">Услуги</a> </li>
                                                <li> <a href="#tw" class="go">Примеры работ</a> </li>
                                                <li> <a href="#map" class="go">Контакты</a> </li>
                                            </ul>
                                        </div>
                                    </div>
                                    <div class="rt">
                                        <a href="tel:+73833350221">
                                            <div class="r sc-content sc-content-_cnt_18722540661414593142">
                                                <p>+7 (383) 335-02-21</p>
                                            </div>
                                        </a>
                                        <a href="#ord" class="ord-menu sc-content sc-content-_cnt_633097464186073807">
                                            <p>Оставить заявку</p>
                                        </a>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- menu-mob -->
                        <div v-if="isMenu" @click="showMenu" class="overlay-menu" v-cloak></div>
                        <div class="header-mob-wrapper" :class="{'mob-menu-show': isMenu}">
                            <div class="mob-close-menu" @click="showMenu">
                                <img src="img/close-menu.svg">
                            </div>
                            <div class="header-mob">
                                <div class="logo-mob sc-content sc-content-_cnt_1711017887814680593">
                                    <img src="img/logo-xs.png">
                                    <div class="logo-text sc-content sc-content-_cnt_161046527563580079">
                                        <p>РСК СибСтальКонструкция</p>
                                        <p>производство монтаж</p>
                                    </div>
                                </div>
                                <div class="menu-mob-box sc-content sc-content-_cnt_21406120871632670455">
                                    <ul class="menu-mob">
                                        <li> <a href="#main" class="go">Главная</a> </li>
                                        <li> <a href="#advantages" class="go">Наши приемущества</a> </li>
                                        <li> <a href="#on" class="go">Услуги</a> </li>
                                        <li> <a href="#tw" class="go">Примеры работ</a> </li>
                                        <li> <a href="#map" class="go">Контакты</a> </li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                        <!-- конец menu-mob -->
                        <div class="head" id="main">
                            <div class="swiper-container sld-main">
                                <div class="swiper-wrapper">
                                    <div v-for="n in mainSl" :key="n.order" class="swiper-slide" v-bind:style="{background: `linear-gradient(90deg, rgba(32, 14, 14, 0.3) 0%, rgba(255, 255, 255, 0) 50%), linear-gradient(270deg, rgba(32, 14, 14, 0.3) 4.44%, rgba(255, 255, 255, 0) 50%), linear-gradient(180deg, rgba(32, 14, 14, 0.7) 0%, rgba(255, 255, 255, 0) 100%), linear-gradient(0deg, rgba(32, 14, 14, 0.7) 0%, rgba(0, 0, 0, 0) 50%), url(/img/main/${n.img}) no-repeat center/cover`}">
                                        <div class="main-title sc-content sc-content-_cnt_1230940950353518360">
                                            <h1>{{n.title}}</h1>
                                            <p>{{n.posttitle}}</p>
                                        </div>
                                    </div>
                                </div>
                                <!-- Add Pagination -->
                                <div class="swiper-pagination"></div>
                                <!-- Add Arrows -->
                                <div class="swiper-button-next"></div>
                                <div class="swiper-button-prev"></div>
                            </div>
                            <!-- <div class="wrap">
                            <div class="on"></div>
                        </div>
                        --> 
                        <div class="btn-ord sc-content sc-content-_cnt_8139575031235977626">
                            <a href="#ord">
                                <p>Оставить заявку</p>
                            </a>
                        </div>
                        <div class="after-red"></div>
                    </div>
                </div>
            </header>
            <section id="advantages" class="wrap">
                <div class="advantages-wrapper">
                    <div class="advantages-box sc-content sc-content-_cnt_7594089081074469350">
                        <img src="img/1adv.png" alt="">
                        <h4>Качественно</h4>
                        <p>Гарантируем высокое качество используемых материалов.</p>
                    </div>
                    <div class="advantages-box sc-content sc-content-_cnt_389711712967673738">
                        <img src="img/2adv.png" alt="">
                        <h4>Быстро</h4>
                        <p>Декларируем сжатые сроки выполнения заказов и их соблюдение.</p>
                    </div>
                    <div class="advantages-box sc-content sc-content-_cnt_656680436967475982">
                        <img src="img/3adv.png" alt="">
                        <h4>Индивидуально</h4>
                        <p>Гарантируем строго индивидуальный подход ко всем клиентам.</p>
                    </div>
                    <div class="advantages-box sc-content sc-content-_cnt_11344822811702451867">
                        <img src="img/4adv.png" alt="">
                        <h4>Без посредников</h4>
                        <p>Возможность изготовления конструкций на нашем производстве.</p>
                    </div>
                    <div class="advantages-box sc-content sc-content-_cnt_11646123361844152575">
                        <img src="img/5adv.png" alt="">
                        <h4>Выгодно</h4>
                        <p>Предлагаем адекватные цены на монтаж металлоконструкций.</p>
                    </div>
                </div>
            </section>
            <section id="on">
                <div class="wrap">
                    <div class="sc-content sc-content-_cnt_1279726456387339901">
                        <h3>Наши услуги</h3>
                    </div>
                    <div class="serv service-wrapper">
                        <div class="service-wr">
                            <div class="service-box sc-content sc-content-_cnt_787212367340885001">
                                <div class="gradient">
                                    <div class="h sc-content sc-content-_cnt_958890373142694231">
                                        <h4>Производство</h4>
                                    </div>
                                    <div class="btn-ord sc-content sc-content-_cnt_14303233541930605560">
                                        <a href="#ord">
                                            <p>Оставить заявку</p>
                                        </a>
                                    </div>
                                </div>
                                <div id="name" class="b sc-content service sc-content-_cnt_19648259531707465324" style="background-image: url(/img/service/1.jpg)"></div>
                                <div class="line sc-content sc-content-_cnt_15900234631436611154">
                                    <a href="#ord" class="post-link">
                                        <p>Оставить заявку</p>
                                    </a>
                                </div>
                            </div>
                        </div>
                        <div class="service-wr">
                            <div class="service-box sc-content sc-content-_cnt_1403545467261409274">
                                <div class="gradient">
                                    <div class="h sc-content sc-content-_cnt_11770071621666608403">
                                        <h4>Монтаж</h4>
                                    </div>
                                    <div class="btn-ord sc-content sc-content-_cnt_2063708971730196198">
                                        <a href="#ord">
                                            <p>Оставить заявку</p>
                                        </a>
                                    </div>
                                </div>
                                <div id="name" class="b sc-content service sc-content-_cnt_19566581761082167315" style="background-image: url(/img/service/2.jpg)"></div>
                                <div class="line sc-content sc-content-_cnt_5509956651279038033">
                                    <a href="#ord" class="post-link">
                                        <p>Оставить заявку</p>
                                    </a>
                                </div>
                            </div>
                        </div>
                        <div class="service-wr">
                            <div class="service-box sc-content sc-content-_cnt_411001228179259574">
                                <div class="gradient">
                                    <div class="h sc-content sc-content-_cnt_134064036661823798">
                                        <h4>Аренда</h4>
                                    </div>
                                    <div class="btn-ord sc-content sc-content-_cnt_1656288253300340993">
                                        <a href="rent.html">
                                            <p>Посмотреть модели</p>
                                        </a>
                                    </div>
                                </div>
                                <div id="name" class="b sc-content service sc-content-_cnt_65944224896125881" style="background-image: url(/img/service/3.jpg)"></div>
                                <div class="line sc-content sc-content-_cnt_1798268332659609460">
                                    <a href="rent.html" data-fancybox data-type="ajax" data-src="rent.html" class="post-link">
                                        <p>Посмотреть модели</p>
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
            <section id="th">
                <div class="wrap type-work">
                    <div class="sc-content sc-content-_cnt_6430766901783075781">
                        <h3>Виды работ</h3>
                    </div>
                    <div class="on">
                        <div class="l sc-content sc-content-_cnt_1290854781966708156">
                            <h4>Ангары любой сложности</h4>
                            <div class="box sc-content sc-content-_cnt_2133776789969086762">
                                <img src="img/adv1-red.png" alt="">
                                <p>В промышленности широко востребованы ангары, являющиеся оптимальным выбором из-за их невысокой себестоимости и быстрой скорости возведения.</p>
                            </div>
                        </div>
                        <div class="l sc-content sc-content-_cnt_1123302259435393988">
                            <h4>Склады и хранилища</h4>
                            <div class="box sc-content sc-content-_cnt_13055445701813720716">
                                <img alt="" style="width:14.98258%;" src="img/adv3-red.png" srcset="img/adv3-red.png 86w">
                                <p>Современные складские помещения любых размеров монтируются с помощью легких металлоконструкций под ключ за считанные дни. </p>
                            </div>
                        </div>
                        <div class="l sc-content sc-content-_cnt_3270859711720946627">
                            <h4>Автоцентры и СТО</h4>
                            <div class="box sc-content sc-content-_cnt_4859288291616640882">
                                <img src="img/car-red.png" alt="">
                                <p>Современный автоцентр - это многофункциональное здание, возводимое в соответствии с требованиями внешнего вида, удобства и надежности. </p>
                            </div>
                        </div>
                        <div class="l sc-content sc-content-_cnt_14918128781557853148">
                            <h4>Торговые павильоны и гаражи</h4>
                            <div class="box sc-content sc-content-_cnt_1326757365327188048">
                                <img src="img/adv4-red.png" alt="">
                                <p>Построить магазин из металлоконструкций, торговый центр или павильон гораздо дешевле, быстрее и проще, чем начинать капитальную стройку. </p>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
            <section id="tw">
                <div class="work">
                    <div class="wrap">
                        <div class="sc-content sc-content-_cnt_2146962254553983641">
                            <h3>Примеры работ</h3>
                        </div>
                        <div class="work-img-wr">
                            <div v-for="(example,index) in examplesWork" :key="example.order" class="on sc-content sc-content-_cnt_19446930522143763282">
                                <div v-bind:style="{background: `url(/img/work/${example.img}) no-repeat right center/cover`}" class="h sc-content sc-content-_cnt_1334231783777830966"></div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
            <section class="partner">
                <div class="wrap">
                    <div class="on sc-content sc-content-_cnt_1978810522359082470">
                        <h3>Наши клиенты</h3>
                        <div class="logo-partners">
                            <div v-for="partner in partners" :key="partner.order" class="logo-pr sc-content sc-content-_cnt_103509942760237652">
                                <a href="#">
                                    <img :src="`img/partners/${partner.img}`" alt="">
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
            <section id="flamp">
                <div class="flamp-wrapper wrap"> <a class="flamp-widget" href="//novosibirsk.flamp.ru/firm/sibstalkonstrukciya_ooo_remontno_stroitelnaya_kompaniya-70000001040998348" data-flamp-widget-type="responsive-new" data-flamp-widget-id="70000001029842003" data-flamp-widget-width="100%" data-flamp-widget-count="1"> Отзывы о нас на Флампе </a> </div>
            </section>
            <section id="form-mob" class="wrap">
                <form action="order.php" method="post">
                    <div class="wrapper">
                        <div class="title-form-box sc-content sc-content-_cnt_1194855082485937509">
                            <h4>Оставьте заявку</h4>
                            <p>Мы свяжемся с Вами в течение 5 минут</p>
                        </div>
                        <div class="form-wrap">
                            <div class="form-wr sc-content sc-content-_cnt_805863871545022419">
                                <div class="form-box sc-content sc-content-_cnt_15970351481391668674">
                                    <input type="text" name="name" placeholder="Имя">
                                </div>
                                <div class="form-box sc-content sc-content-_cnt_335702246493746366">
                                    <input type="text" name="phone" inputmode="numeric" placeholder="+7 (999) 999 99 99">
                                </div>
                                <div class="btn-form-box">
                                    <input type="submit" value="Отправить заявку" class="but">
                                </div>
                            </div>
                            <label>Отправляя форму, я даю свое согласие на обработку <a href="policy.php" target="_blank" data-fancybox data-type="ajax" data-filter=".policy" data-src="policy.php">персональных данных</a> </label>
                        </div>
                    </div>
                </form>
            </section>
            <section id="th">
                <div class="contact">
                    <div class="on map">
                        <div class="map-wr">
                            <div id="map" style="width:100%;height:450px"></div>
                        </div>
                        <div class="contact-wr sc-content sc-content-_cnt_2044760615306074731">
                            <h3>Контакты</h3>
                            <div class="cont-box sc-content sc-content-_cnt_1132912498926526438">
                                <p class="title">Адрес:</p>
                                <p class="text">Новосибирск, ул.Бетонная, 16а</p>
                            </div>
                            <div class="cont-box sc-content sc-content-_cnt_2063694452921987656">
                                <p class="title">График работы:</p>
                                <p class="text">ПН-ПТ с 8:00 до 17:00</p>
                            </div>
                            <div class="cont-box sc-content sc-content-_cnt_7125399621842274354">
                                <p class="title">Телефон:</p>
                                <p class="text"><a href="tel:+73833350221">+7 (383) 335‒02‒21</a></p>
                            </div>
                            <div class="cont-box sc-content sc-content-_cnt_433024338311014836">
                                <p class="title">Почта:</p>
                                <p class="text"><a href="mailto:rsk_ssk@mail.ru">rsk_ssk@mail.ru</a></p>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
            <footer>
                <div class="foot">
                    <div class="wrap">
                        <div class="on">
                            <div class="lf sc-content sc-content-_cnt_109954725260037491">
                                <img src="img/logo-xs.png">
                                <div class="logo-text sc-content sc-content-_cnt_770990138913660400">
                                    <p>РСК СибСтальКонструкция</p>
                                    <p>производство монтаж</p>
                                </div>
                            </div>
                            <div class="cn sc-content sc-content-_cnt_1325851579940370962">
                                <ul class="footer-menu">
                                    <li> <a href="#main" class="go">Главная</a> </li>
                                    <li> <a href="#advantages" class="go">Наши приемущества</a> </li>
                                    <li> <a href="#on" class="go">Услуги</a> </li>
                                </ul>
                                <ul class="footer-menu">
                                    <li> <a href="#tw" class="go">Примеры работ</a> </li>
                                    <li> <a href="#map" class="go">Контакты</a> </li>
                                    <li> <a href="policy.php" target="_blank" data-fancybox data-type="ajax" data-filter=".policy" data-src="policy.php">Политика кондифициальности</a> </li>
                                </ul>
                            </div>
                        </div>
                        <div class="on footer-sib">
                            <div class="lf">
                                <p>
                                    &copy; 
                                    <?php echo date('Y').' '. $_SERVER['HTTP_HOST'] ?>
                                    </p>
                                </div>
                                <div class="rt lf">
                                    <p>Сделано в <a href="//sibrix.ru" target="_blank">Сибрикс</a></p>
                                </div>
                            </div>
                        </div>
                    </div>
                </footer>
                <div class="mess">
                    <div class="a sc-content sc-content-_cnt_5378022571236354069">
                        <a href="//wa.me/79137253296" target="_blank">
                            <img src="img/ws.png">
                        </a>
                    </div>
                </div>
                <div class="modal" style="display:none">
                    <div class="on">
                        <div class="a sc-content sc-content-_cnt_980393201714554901">
                            <h4>Оставить заявку</h4>
                            <p>Заполните поля ниже, мы свяжемся с Вами в течение 5 минут</p>
                        </div>
                        <form action="order.php" method="post">
                            <input type="text" name="name" placeholder="Имя">
                            <input type="text" name="phone" inputmode="numeric" placeholder="+7 (999) 999 99 99">
                            <input type="text" name="ord" style="display:none">
                            <input type="text" name="mail">
                            <label>Отправляя форму, я даю свое согласие на обработку <a href="policy.php" target="_blank" data-fancybox data-type="ajax" data-filter=".policy" data-src="policy.php">персональных данных</a></label>
                            <input type="submit" value="Отправить заявку" class="but">
                        </form>
                    </div>
                </div>
                <!-- слайдер для примеров в попапе -->
                <div v-if="isExample" @click.self="closeExample()" class="popup-overlay" v-cloak>
                    <div @click="closeExample()" class="popup-close">
                        <img src="img/close-menu.svg">
                    </div>
                    <div :class="{'show-h': isExample }">
                        <div :style="{background: `url(/img/work/${currentImg}.jpg) no-repeat right center/cover`}" class="h"></div>
                    </div>
                    <div @click="nextExample(currentImg)" class="swiper-button-next"></div>
                    <div @click="prevExample(currentImg)" class="swiper-button-prev"></div>
                </div>
                <!-- конец слайдер -->
            </div>
            <script src="js/framework.js" type="text/javascript"></script>
        </body>
    </html>